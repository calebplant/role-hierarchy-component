public with sharing class RoleUtils {

    private static Map<Id, List<UserRole>> rolesByParentId;

    public static List<RoleNode> getCompleteRoleHierarchy()
    {
        List<RoleNode> result = checkCacheForHierarchy();
        if(result != null) {
            System.debug('Cache hit.');
            return result;
        }

        System.debug('Cache miss.');
        result = buildRoleHierarchy();
        setCachedHierarchy(result);
        return result;
    }

    public static List<RoleNode> buildRoleHierarchy()
    {
        List<RoleNode> roleTree = new List<RoleNode>();

        // Map roles by their parent's Id
        List<UserRole> allRoles = [SELECT Id, Name, ParentRoleId FROM UserRole];
        rolesByParentId = new Map<Id, List<UserRole>>();
        for(UserRole eachRole : allRoles) {
            List<UserRole> existingRoles = rolesByParentId.get(eachRole.ParentRoleId);
            if(existingRoles != null) {
                existingRoles.add(eachRole);
            } else {
                existingRoles = new List<UserRole>{eachRole};
            }
            rolesByParentId.put(eachRole.ParentRoleId, existingRoles);
        }

        // Connect roles in tree hierarchy
        List<UserRole> topLevelRoles = rolesByParentId.get(null);
        for(UserRole eachTopRole : topLevelRoles) {
            roleTree.addAll(buildSubordinateNodes(eachTopRole));
        }

        return roleTree;
    }

    private static List<RoleNode> buildSubordinateNodes(UserRole rootRole)
    {
        RoleNode rootNode = new RoleNode(rootRole);
        List<RoleNode> result = new List<RoleNode>{addChildrenNodes(rootNode)};
        return result;
    }

    private static RoleNode addChildrenNodes(RoleNode node)
    {
        List<UserRole> subordinateRoles = rolesByParentId.get(node.name);
        if(subordinateRoles != null) {
            for(UserRole eachRole : subordinateRoles) {
                RoleNode newChild = new RoleNode(eachRole);
                node.items.add(addChildrenNodes(newChild));
            }
        }
        return node;
    }

    private static List<RoleNode> checkCacheForHierarchy()
    {
        Cache.OrgPartition orgPart = Cache.Org.getPartition('local.RoleHierarchyCache');
        return (List<RoleNode>)orgPart.get('RoleHierarchy');
    }

    
    private static void setCachedHierarchy(List<RoleNode> updatedHierarchy)
    {
        Cache.OrgPartition orgPart = Cache.Org.getPartition('local.RoleHierarchyCache');
        orgPart.put('RoleHierarchy', updatedHierarchy, 300);
    }


    // public static void refreshCachedHierarchy()
    // {
    //     List<RoleNode> updatedHierarchy = buildCompleteHierarchy();
    //     Cache.OrgPartition orgPart = Cache.Org.getPartition('local.RoleHierarchyCache');
    //     orgPart.put('RoleHierarchy', updatedHierarchy);
    // }
}
