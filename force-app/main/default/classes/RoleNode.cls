public class RoleNode {

    @AuraEnabled
    public String label;
    @AuraEnabled
    public Id name;
    @AuraEnabled
    public Boolean expanded;
    @AuraEnabled
    public List<RoleNode> items;

    public RoleNode(UserRole role)
    {
        this.label = role.Name;
        this.name = role.Id;
        this.expanded = false;
        this.items = new List<RoleNode>();
    }

    public RoleNode() {}
}