public with sharing class RoleHierarchyController {

    @AuraEnabled(cacheable=true)
    public static List<UserRole> getAllRoles()
    {
        return [SELECT Id, Name FROM UserRole
                ORDER BY Name];
    }
    
    @AuraEnabled(cacheable=true)
    public static List<RoleNode> getCompleteRoleHierarchy()
    {
        return RoleUtils.getCompleteRoleHierarchy();
    }
}
