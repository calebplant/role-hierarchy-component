@isTest
public with sharing class RoleHierarchyController_Test {

    @isTest
    static void doesGetAllRolesReturnAllRoles()
    {
        Integer numOfRoles = [SELECT COUNT() FROM UserRole];

        Test.startTest();
        List<UserRole> result = RoleHierarchyController.getAllRoles();
        Test.stopTest();

        System.assertEquals(numOfRoles, result.size());
    }

    @isTest
    static void doesGetCompleteRoleHierarchyReturnCompleteTree()
    {
        /* SEE RoleUtil_Test class for functionality testing. */
        Test.startTest();
        List<RoleNode> result = RoleHierarchyController.getCompleteRoleHierarchy();
        Test.stopTest();
    }

}
