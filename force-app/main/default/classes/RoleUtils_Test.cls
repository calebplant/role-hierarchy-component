@isTest
public with sharing class RoleUtils_Test {
    
    private static String ROOT_ROLE_NAME = 'root';
    private static String CHILD_ONE_ROLE_NAME = 'childOne';
    private static String CHILD_TWO_ROLE_NAME = 'childTwo';
    private static String GRANDCHILD_ONE_ROLE_NAME = 'grandChildOne';

    @TestSetup
    static void makeData(){
        // root -> childOne -> grandChildOne
        UserRole root = new UserRole(Name=ROOT_ROLE_NAME);
        insert root;
        UserRole childOne = new UserRole(Name=CHILD_ONE_ROLE_NAME, ParentRoleId=root.Id);
        insert ChildOne;
        UserRole grandChildOne = new UserRole(Name=GRANDCHILD_ONE_ROLE_NAME, ParentRoleId=childOne.Id);
        insert grandChildOne;
        // root -> childTwo
        UserRole childTwo = new UserRole(Name=CHILD_TWO_ROLE_NAME, ParentRoleId=root.Id);
        insert childTwo;
    }

    @isTest
    static void doesGetCompleteRoleHierarchyReturnCorrectStructure()
    {
        Test.startTest();
        List<RoleNode> result = RoleUtils.getCompleteRoleHierarchy();
        Test.stopTest();

        // Get test-created root role
        RoleNode resultRootNode = new RoleNode();
        for(RoleNode eachTopLevelNode : result) {
            if(eachTopLevelNode.label == ROOT_ROLE_NAME) {
                resultRootNode = eachTopLevelNode;
            }
        }
        System.assertEquals(ROOT_ROLE_NAME, resultRootNode.label, 'root did not have correct label');
        System.assertEquals(2, resultRootNode.items.size(), 'root did not have correct number of children');
        // Get grandchild node
        RoleNode resultChildOneNode = new RoleNode();
        for(RoleNode eachChildNode : resultRootNode.items) {
            if(eachChildNode.items.size() > 0) {
                resultChildOneNode = eachChildNode;
            }
        }
        System.assertEquals(GRANDCHILD_ONE_ROLE_NAME, resultChildOneNode.items[0].label, 'grandchild node did not have correct label');
    }

    @isTest
    static void doesGetCompleteRoleHierarchyUseCacheIfAvailable()
    {
        String cacheNodeLabel = 'bananas';

        RoleNode cacheNode = new RoleNode();
        cacheNode.label = cacheNodeLabel;
        List<RoleNode> cacheHierarchy = new List<RoleNode>{cacheNode};

        Cache.OrgPartition orgPart = Cache.Org.getPartition('local.RoleHierarchyCache');
        orgPart.put('RoleHierarchy', cacheHierarchy);

        Test.startTest();
        List<RoleNode> result = RoleUtils.getCompleteRoleHierarchy();
        Test.stopTest();

        System.assertEquals(cacheNodeLabel, result[0].label);
    }
}
