import { LightningElement, wire } from 'lwc';
import getAllRoles from '@salesforce/apex/RoleHierarchyController.getAllRoles';
import getCompleteRoleHierarchy from '@salesforce/apex/RoleHierarchyController.getCompleteRoleHierarchy';

export default class RoleHierarchyComponent extends LightningElement {

    selectedRootRole;
    roleOptions;
    roleHierarchy;
    isLoadingHierarchy = true;

    
    @wire(getAllRoles)
    getRoles(response) {
        if(response.data) {
            console.log('Successfully retreived roles');
            let foundRoles = response.data.map(eachRole => {
                return { label: eachRole.Name, value: eachRole.Id}
            });
            foundRoles.unshift({label: '', value: undefined})
            this.roleOptions = foundRoles;
        }
        if(response.error) {
            console.log('Error retreiving roles');
            console.log(response.error);
        }
    }

    @wire(getCompleteRoleHierarchy)
    getHierarchy(response) {
        if(response.data) {
            console.log('Successfully retreived hierarchy');
            console.log(JSON.parse(JSON.stringify(response.data)));
            this.roleHierarchy = response.data;
            this.isLoadingHierarchy = false;
        }
        if(response.error) {
            console.log('Error retreiving hierarchy');
            console.log(response.error);
        }
    }

    get displayedHierarchy() {
        let stack = []
        let node;
        let i;
        let j;

        if(!this.selectedRootRole) {
            console.log('No selected root.');
            return this.roleHierarchy;
        }

        for(i = 0; i < this.roleHierarchy.length; i++) {
            stack.push(this.roleHierarchy[i]);

            while (stack.length > 0) {
                node = stack.pop();
                if (node.name == this.selectedRootRole) {
                    console.log('Found node: ' + node.name);
                    return [node];
                } else if (node.items && node.items.length) {
                    for (j = 0; j < node.items.length; j += 1) {
                        stack.push(node.items[j]);
                    }
                }
            }
        }
        console.log('Root not found. Returning all.');
        return this.roleHierarchy;
    }


    // Handlers
    handleRootSelect(event) {
        this.selectedRootRole = event.detail.value;
        console.log('New root selected:');
        console.log(JSON.parse(JSON.stringify(this.selectedRootRole)));
    }
}