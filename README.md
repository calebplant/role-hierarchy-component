# Role Hierarchy Component

## Overview

Component allowing users to view a hierarchal tree of the org's user roles. Users can alternatively select the root node that they want to view and see all subordinate roles in a hierarchal view.

## Demo

### Video (~40 sec)

[Here's a link to a demo video for the component](https://www.youtube.com/watch?v=uHXvze1Glhk)

### Basic Functionality

![Basic Functionality](media/basic_functionality.gif)

### Selecting a Root

![Selecting a Root](media/selecting_root.gif)

## File Overview

### LWC

* **roleHierarchyComponent** - Stand-alone component that retrieves the org's Roles and displays them in a tree. Users can expand/collapse roles and select a root role.

### Apex

* **RoleHierarchyController** - Controller for the Role Hierarchy Component
* **RoleUtils** - Used by the Controller. Retrieves the role hierarchy from the platform cache if it exists; if hierarchy is not stored in cache, builds and returns the hierarchy and records it in the platform cache.